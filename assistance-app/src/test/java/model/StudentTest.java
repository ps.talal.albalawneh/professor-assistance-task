package model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class StudentTest {

    @Test
    void givenStudent_whenSetterOrGetter_thenGetOrGiveValue(){
        String id="520", classNo = "B", mark="75", zScore = "45.3", category = "Pass";
        Assertions.assertDoesNotThrow(()->{
            Student student = new Student("123","A", "70");
            student.setStudentId(id);
            Assertions.assertEquals(id, student.getStudentId());
            student.setClassNo(classNo);
            Assertions.assertEquals(classNo, student.getClassNo());
            student.setMark(mark);
            Assertions.assertEquals(mark, student.getMark());
            student.setzScore(zScore);
            Assertions.assertEquals(zScore, student.getzScore());
            student.setCategory(category);
            Assertions.assertEquals(category, student.getCategory());

            student = new Student(id, classNo, mark, zScore, category);
        });
    }

}