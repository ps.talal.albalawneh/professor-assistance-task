package utility;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;

class DataReaderTest {
    final String TXT_PATH = "/home/talal/IdeaProjects/Professor-Assistance/generated-files/txt_generated_file.txt";
    final String NOT_EXIST_TXT_PATH = "/home/talal/IdeaProjects/Professor-Assistance/generated-files/txt_generated_file2.txt";
    final String DIRECTORY_TXT_PATH = "/home/talal/IdeaProjects/Professor-Assistance/generated-files/";
    final String CSV_PATH = "/home/talal/IdeaProjects/Professor-Assistance/generated-files/generated_file.csv";
    @Test
    void givenNullDataSupplier_whenInitializeDataReader_thenExceptionThrown() {
        Exception exception = Assertions.assertThrows(NullPointerException.class, () -> {
            new DataReader(null);
        });
        Assertions.assertEquals(NullPointerException.class, exception.getClass());
        Assertions.assertEquals("Null Supplier.", exception.getMessage());
    }

    @Test
    void givenNotNullSupplier_whenInitializeDataReader_thenItsGoingFine() {
        Assertions.assertDoesNotThrow(() -> {
            new DataReader(new CSVFile(""));
        });
    }

    @Test
    void givenNotNullSupplier_whenRead_thenFileContentReturned() throws IOException {
        String txt_path = "src/test/resources/testing-file/read-test.txt";
        String csv_path = "src/test/resources/testing-file/csv-read-test.csv";
        DataReader txtReader = new DataReader(new TxtFile(txt_path));
        String expected = "hello from the test file.\n";
        String actual = txtReader.read();
        Assertions.assertEquals(expected, actual);

        DataReader csvReader = new DataReader(new CSVFile(csv_path));
        expected = "hello from the test file.\n";
        actual = csvReader.read();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void givenNullFileName_whenCSVWrite_thenExceptionThrown() {

        DataReader dr = new DataReader(new CSVFile(CSV_PATH));

        Exception ex = Assertions.assertThrows(NullPointerException.class, () -> {
            dr.write("000", null);
        });
        Assertions.assertEquals(NullPointerException.class, ex.getClass());
        Assertions.assertEquals("Null file name not allowed.", ex.getMessage());
    }

    @Test
    void givenNullFileName_whenTXTWrite_thenExceptionThrown() {

        DataReader dr = new DataReader(new TxtFile(TXT_PATH));

        Exception ex = Assertions.assertThrows(NullPointerException.class, () -> {
            dr.write("000", null);
        });
        Assertions.assertEquals(NullPointerException.class, ex.getClass());
        Assertions.assertEquals("Null file name not allowed.", ex.getMessage());
    }

    @Test
    void givenValidFileName_whenWrite_thenNoExceptionIsThrown() {
        DataReader dr = new DataReader(new TxtFile("src/test/resources/testing-file/read-test.txt"));
        Assertions.assertDoesNotThrow(() -> {
            dr.write("new data", "read-test_txt");
        });

        DataReader dr1 = new DataReader(new CSVFile("src/test/resources/testing-file/read-test.txt"));
        Assertions.assertDoesNotThrow(() -> {
            dr.write("new data", "read-test_csv");
        });

    }

}