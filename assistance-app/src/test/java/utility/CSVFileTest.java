package utility;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CSVFileTest {
    final String NOT_EXIST_CSV_PATH = "/home/talal/IdeaProjects/Professor-Assistance/generated-files/csv_generated_file2.txt";
    final String DIRECTORY_CSV_PATH = "/home/talal/IdeaProjects/Professor-Assistance/generated-files/";

    @Test
    void givenNotExistTxtFilePath_whenUsed_thenExceptionThrown(){
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, ()->{
            CSVFile file = new CSVFile(NOT_EXIST_CSV_PATH);
            file.read();
        });
        Assertions.assertEquals("file not found.", exception.getMessage());
    }

    @Test
    void givenDirectoryTxtFilePath_whenUsed_thenExceptionThrown(){
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, ()->{
            CSVFile file = new CSVFile(DIRECTORY_CSV_PATH);
            file.read();
        });
        Assertions.assertEquals("wrong file path.", exception.getMessage());
    }

    @Test
    void givenNullTxtFilePath_whenUsed_thenExceptionThrown(){
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, ()->{
            CSVFile file = new CSVFile(null);
        });
        Assertions.assertEquals("Null file not allowed.", exception.getMessage());
    }

}