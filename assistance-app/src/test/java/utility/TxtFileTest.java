package utility;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TxtFileTest {
    final String TXT_PATH = "/home/talal/IdeaProjects/Professor-Assistance/generated-files/txt_generated_file.txt";
    final String NOT_EXIST_TXT_PATH = "/home/talal/IdeaProjects/Professor-Assistance/generated-files/txt_generated_file2.txt";
    final String DIRECTORY_TXT_PATH = "/home/talal/IdeaProjects/Professor-Assistance/generated-files/";

    @Test
    void givenNotExistTxtFilePath_whenUsed_thenExceptionThrown(){
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, ()->{
            TxtFile file = new TxtFile(NOT_EXIST_TXT_PATH);
            file.read();
        });
        Assertions.assertEquals("file not found.", exception.getMessage());
    }

    @Test
    void givenDirectoryTxtFilePath_whenUsed_thenExceptionThrown(){
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, ()->{
            TxtFile file = new TxtFile(DIRECTORY_TXT_PATH);
            file.read();
        });
        Assertions.assertEquals("wrong file path.", exception.getMessage());
    }

    @Test
    void givenNullTxtFilePath_whenUsed_thenExceptionThrown(){
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, ()->{
            TxtFile file = new TxtFile(null);
        });
        Assertions.assertEquals("Null file not allowed.", exception.getMessage());
    }

}