package tools;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

class SummaryTest {
    ArrayList<BigDecimal> data = new ArrayList<>();
    Summary summary;

    @BeforeEach
    void setupData() {
        for (int i = 1; i <= 10; i++) {
            data.add(new BigDecimal(i));
        }
        summary = new Summary(data);
    }

    @Test
    void givenData_whenMean_thenValueReturned() {
        BigDecimal expected = new BigDecimal(5.5).setScale(2);
        BigDecimal actual = summary.mean();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void givenData_whenMedian_thenValueReturned() {
        BigDecimal expected = new BigDecimal(5.5).setScale(2);
        BigDecimal actual = summary.median();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void givenData_whenStandardDeviation_thenValueReturned() {
        BigDecimal expected = new BigDecimal(2.872).setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal actual = summary.standardDeviation();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void givenData_whenVariance_thenValueReturned() {
        BigDecimal expected = new BigDecimal(8.25).setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal actual = summary.variance();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void givenData_whenTotalCount_thenValueReturned() {
        BigDecimal count = summary.totalCount();
        BigDecimal expected = new BigDecimal(10);
        Assertions.assertEquals(expected, count);
    }

    @Test
    void givenData_whenZScore_thenValueReturned(){
        BigDecimal expected = new BigDecimal(0.1712).setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal actual = summary.zScore(data.get(5).toString());
        Assertions.assertEquals(expected, actual);
    }
}