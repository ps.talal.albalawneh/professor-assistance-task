package model;

public class Student {
    // TODO naming convention, remove the _ --> Done
    // TODO Methods naming convention, remove the _ --> Done
    private String studentId;
    private String classNo;
    private String mark;
    private String zScore;
    private String category;

    public Student(String studentId, String classNo, String mark) {
        this.studentId = studentId;
        this.classNo = classNo;
        this.mark = mark;
    }

    public Student(String studentId, String classNo, String mark, String zScore, String category) {
        this.studentId = studentId;
        this.classNo = classNo;
        this.mark = mark;
        this.zScore = zScore;
        this.category = category;
    }

    public String getzScore() {
        return zScore;
    }

    public void setzScore(String zScore) {
        this.zScore = zScore;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getClassNo() {
        return classNo;
    }

    public void setClassNo(String classNo) {
        this.classNo = classNo;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }
}
