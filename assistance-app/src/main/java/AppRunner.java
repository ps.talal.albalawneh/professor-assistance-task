import model.Student;
import tools.Summary;
import utility.CSVFile;
import utility.DataReader;
import utility.DataSupplier;
import utility.TxtFile;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

public class AppRunner {
    // TODO, add access modifiers to the variables --> Done
    // Optional TODO, it is recommended to have the objects
    private ArrayList<Student> students;
    private ArrayList<String> headers;
    private HashMap<String, ArrayList<Student>> categorizedStudents;
    private DataReader dataReader;
    private String delimiter = null;


    public void run() throws IOException {
        int dataSrc = askUserForSourceOfData(new Scanner(System.in));
        String data = getData(dataSrc);
        reStructureData(data, delimiter);
        loop();
    }

    private String getData(int ds) throws IOException {
        String path;
        switch (ds) {
            case 1:
                path = askForFilePath(new Scanner(System.in));
                CSVFile csvFile = new CSVFile(path);
                delimiter = ",";
                return getCsvOrTxtData(csvFile);
            case 2:
                path = askForFilePath(new Scanner(System.in));
                TxtFile txtFile = new TxtFile(path);
                delimiter = askForDelimiter(new Scanner(System.in));
                return getCsvOrTxtData(txtFile);
            default:
                throw new IllegalArgumentException("No such choice " + ds);
        }
    }

    private String askForDelimiter(Scanner sc) {
        System.out.print("Input delimiter: ");
        return sc.nextLine();
    }

    private void reStructureData(String data, String delimiter) {
        headers = new ArrayList<>();
        students = new ArrayList<>();
        categorizedStudents = new HashMap<>();
        ArrayList<String> lines = new ArrayList<>(Arrays.asList(data.split("\n")));
        headers.addAll(Arrays.asList(lines.get(0).split(delimiter)));
        for (int i = 1; i < lines.size(); i++) {
            String[] row = lines.get(i).split(delimiter);
            Student student = new Student(row[0], row[1], row[2]);
            students.add(student);
            if (!categorizedStudents.containsKey(student.getClassNo())) {
                ArrayList<Student> temp = new ArrayList<>();
                categorizedStudents.put(student.getClassNo(), temp);
            }
            categorizedStudents.get(student.getClassNo()).add(student);
        }
    }

    private String askForFilePath(Scanner sc) {
        System.out.print("Input File Path: ");
        return sc.nextLine();
    }

    private String getCsvOrTxtData(DataSupplier dataSupplier) throws IOException {
        dataReader = new DataReader(dataSupplier);
        return dataReader.read();
    }

    private int askUserForSourceOfData(Scanner sc) {
        System.out.println("Choose data source:");
        System.out.println("1- Csv file.");
        System.out.println("2- Text file.");
        System.out.print("Your Choice: ");
        return sc.nextInt();
    }

    private void loop() throws IOException {
        Scanner sc = new Scanner(System.in);
        int selected = -1;
        // TODO, remove the while loop, and call the loop() method if the selected is not 7 --> Done
        doSeparator();
        System.out.println("Choose one:\n" +
                "1- Summary\n" +
                "2- Summary for a specific class\n" +
                "3- Display Z-scores\n" +
                "4- Display Z-scores for specific class\n" +
                "5- Categorize students\n" +
                "6- Categorize students in a specific class\n" +
                "7- Exit");
        System.out.print("your choice: ");
        selected = sc.nextInt();
        if (selected != 7) {
            doAction(selected);
            loop();
        }
    }

    private void doAction(int selected) throws IOException {
        switch (selected) {
            case 1: {
                summary();
                break;
            }
            case 2: {
                summaryForSpecificClass(askForSpecificClass(new Scanner(System.in)));
                break;
            }
            case 3: {
                String dataWithZScore = displayZScore(students);
                System.out.println(dataWithZScore);
                break;
            }
            case 4: {
                String dataWithZScore = displayZScoreForSpecificClass(askForSpecificClass(new Scanner(System.in)));
                System.out.println(dataWithZScore);
                break;
            }
            case 5: {
                String dataWithZScoreAndCategory = displayStudentCategory(students,
                        askForGradeDev(new Scanner(System.in), "Elite"),
                        askForGradeDev(new Scanner(System.in), "Failed"));
                System.out.println(dataWithZScoreAndCategory);
                if (saveToFile(new Scanner(System.in))) {
                    String name = askForFileName(new Scanner(System.in));
                    dataReader.write(dataWithZScoreAndCategory, name);
                }
                break;
            }
            case 6: {
                String dataWithZScoreAndCategory = displayStudentCategoryForSpecificClass(askForSpecificClass(new Scanner(System.in)));
                System.out.println(dataWithZScoreAndCategory);
                if (saveToFile(new Scanner(System.in))) {
                    String name = askForFileName(new Scanner(System.in));
                    dataReader.write(dataWithZScoreAndCategory, name);
                }
                return;
            }
            default: {
                throw new IllegalArgumentException("No Such Choice " + selected);
            }
        }
    }

    private String askForFileName(Scanner sc) {
        System.out.println("Enter file name: ");
        return sc.nextLine();
    }

    private boolean saveToFile(Scanner sc) {
        System.out.print("Do you want to save the results to a file (yes/no): ");
        return sc.nextLine().equalsIgnoreCase("yes");
    }

    private String displayStudentCategoryForSpecificClass(String category) {
        return displayStudentCategory(categorizedStudents.get(category),
                askForGradeDev(new Scanner(System.in), "Elite"),
                askForGradeDev(new Scanner(System.in), "Failed"));
    }

    private String displayStudentCategory(ArrayList<Student> list, double elite, double failed) {
        int eliteNO = 0, passNo = 0, failNo = 0;
        Summary summary = new Summary(getIntValues(list));
        String dataWithZScore = displayZScore(list);
        String[] splitDataWithZScore = dataWithZScore.split("\n");
        StringBuilder result = new StringBuilder();
        // Optional TODO, change the + delimiter + "" into append --> Done
        result.append(splitDataWithZScore[0]).append(delimiter).append("category");

        for (int i = 0; i < list.size(); i++) {
            double mark = Double.parseDouble(students.get(i).getMark());
            String category = (mark >= elite ? "Elite" : (mark <= failed) ? "Failed" : "Passed");

            if (category.equals("Elite")) eliteNO++;
            else if (category.equals("Passed")) passNo++;
            else failNo++;
            result.append("\n");
            result.append(splitDataWithZScore[i + 1]).append(delimiter).append(category);
        }
        doSeparator();
        System.out.println("Median: " + summary.median() + "\n" +
                "Variance: " + summary.variance() + "\n" +
                "Standard Deviation: " + summary.standardDeviation() + "\n" +
                "Elite students count: " + eliteNO + "\n" +
                "Passed students count: " + passNo + "\n" +
                "Failed students count: " + failNo + "\n" +
                "Passing score: " + (failed + 1) + "\n" +
                "Elite score: " + elite);
        return result.toString();
    }

    // TODO reduce duplicates --> Done
    private double askForGradeDev(Scanner sc, String Grade) {
        System.out.print("Input " + Grade + " Deviations: ");
        return sc.nextDouble();
    }

    private String displayZScoreForSpecificClass(String category) {
        if (!categorizedStudents.containsKey(category)) {
            throw new IllegalArgumentException("No such class " + category);
        }
        return displayZScore(categorizedStudents.get(category));
    }

    private String displayZScore(ArrayList<Student> studentsList) {
        //todo -> change String obj to stringBuilder, do it  --> Done
        StringBuilder result = new StringBuilder();
        Summary summary = new Summary(getIntValues(studentsList));
        for (String header : headers) {
            result.append(header).append(delimiter);
        }
        result.append("z-score\n");

        for (Student student : studentsList) {
            result.append(student.getStudentId()).append(delimiter)
                    .append(student.getClassNo()).append(delimiter)
                    .append(student.getMark()).append(delimiter)
                    .append(summary.zScore(student.getMark()))
                    .append("\n");
        }
        return result.toString();
    }


    private String askForSpecificClass(Scanner sc) {
        doSeparator();
        System.out.print("Classes: ");
        for (String s : categorizedStudents.keySet()) {
            System.out.print(s + " ");
        }
        System.out.print("\nWhich class would you show: ");
        sc = new Scanner(System.in);
        return sc.nextLine().toUpperCase();
    }

    private void summaryForSpecificClass(String category) {
        if (!categorizedStudents.containsKey(category)) {
            throw new IllegalArgumentException("No such class " + category);
        }
        Summary summary = new Summary(getIntValues(categorizedStudents.get(category)));
        printSummery(summary);
    }

    private void printSummery(Summary summary) {
        System.out.println("Median: " + summary.median() + "\n" +
                "Variance: " + summary.variance() + "\n" +
                "Standard Deviation: " + summary.standardDeviation() + "\n" +
                "Total count: " + summary.totalCount());
    }

    private void summary() {
        Summary summary = new Summary(getIntValues(students));
        doSeparator();
        printSummery(summary);
    }

    private void doSeparator() {
        System.out.println();
        for (int i = 0; i < 50; i++) {
            System.out.print("--");
        }
        System.out.println();
    }

    private ArrayList<BigDecimal> getIntValues(ArrayList<Student> list) {
        ArrayList<BigDecimal> res = new ArrayList<>();
        for (Student student : list)
            res.add(new BigDecimal(student.getMark()));

        return res;
    }

}