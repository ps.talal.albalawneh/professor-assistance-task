package tools;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;

public class Summary {
    // TODO, Change it into ArrayList instead of Object[] --> Done
    private ArrayList<BigDecimal> colValues;
    private final BigDecimal numOfValues;
    // hooks to store calculated values
    private BigDecimal mean = null;
    private BigDecimal standardDeviation = null;
    private BigDecimal variance = null;
    private BigDecimal median = null;

    public Summary(ArrayList<BigDecimal> colValues) {
        this.colValues = colValues;
        numOfValues = new BigDecimal(colValues.size());
    }

    public BigDecimal mean() {
        if (isNotCalculated(mean)) {
            BigDecimal sumOfValues = new BigDecimal(0);
            for (BigDecimal value : colValues)
                sumOfValues = sumOfValues.add(value);
            mean = sumOfValues.divide(numOfValues, MathContext.DECIMAL128);
        }
        return reScaleNum(mean);
    }

    public BigDecimal standardDeviation() {
        if (isNotCalculated(standardDeviation)) {
            if (isNotCalculated(mean)) mean = mean();
            BigDecimal deviationsOfValues = new BigDecimal(0);
            for (BigDecimal value : colValues)
                deviationsOfValues = deviationsOfValues.add((value.subtract(mean)).pow(2));

            MathContext mathContext = new MathContext(10);
            standardDeviation = deviationsOfValues.divide(numOfValues,MathContext.DECIMAL128).sqrt(mathContext);
        }
        return reScaleNum(standardDeviation);
    }

    public BigDecimal variance() {
        if (isNotCalculated(variance)) {
            if (isNotCalculated(standardDeviation)) standardDeviation();
            variance = reScaleNum(standardDeviation.pow(2));
        }
        return variance;
    }

    public BigDecimal median() {
        if (isNotCalculated(median)) {
            if (colValues.size() % 2 != 0) {
                median = colValues.get(colValues.size() / 2);
            } else {
                median = (colValues.get(colValues.size() / 2))
                        .add(colValues.get(colValues.size() / 2 - 1))
                        .divide(BigDecimal.valueOf(2), MathContext.DECIMAL128);
            }
        }
        return reScaleNum(median);
    }

    public BigDecimal totalCount() {
        return numOfValues;
    }

    private boolean isNotCalculated(BigDecimal num) {
        // TODO, return num == null instead of the if condition --> Done
        return (num == null);
    }

    // TODO move the calcZScore inside the summary --> Done
    public BigDecimal zScore(String mark) {
        BigDecimal markBD = new BigDecimal(mark);
        BigDecimal result = markBD.subtract(mean());
        result = result.divide(standardDeviation(), MathContext.DECIMAL128);
        return reScaleNum(result);
    }

    private BigDecimal reScaleNum(BigDecimal num) {
        return num.setScale(2, RoundingMode.HALF_EVEN);
    }
}