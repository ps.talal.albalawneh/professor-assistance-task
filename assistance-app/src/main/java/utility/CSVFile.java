package utility;

public class CSVFile implements DataSupplier {
    private String path;

    public CSVFile(String path) {
        FileUtils.failIfNull(path);
        this.path = path;
    }

    @Override
    public String readPath(){
        return path;
    }

    @Override
    public String getExtension() {
        return ".csv";
    }

}
