package utility;

// TODO Create an interface for the Files, that has the method as "default" to avoid duplication --> Done
public class TxtFile implements DataSupplier {
    // TODO, change the naming convention of PATH from constant into a changeable variable  --> Done
    private String path;

    public TxtFile(String path) {
        FileUtils.failIfNull(path);
        this.path = path;
    }

    // TODO check the first TODO --> Done
    @Override
    public String readPath(){
        return path;
    }


    // TODO check the first TODO --> Done
    @Override
    public String getExtension() {
        return ".txt";
    }

}
