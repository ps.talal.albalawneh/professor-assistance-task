package utility;

import java.io.IOException;

public class DataReader {
    private DataSupplier dataSupplier;

    public DataReader(DataSupplier dataSupplier) {
        if (dataSupplier == null)
            throw new NullPointerException("Null Supplier.");
        this.dataSupplier = dataSupplier;
    }

    public String read() throws IOException {
        return dataSupplier.read();
    }

    public void write(String data, String fileName) throws IOException {
        dataSupplier.write(data, fileName);
    }
}
