package utility;

import java.io.File;

public class FileUtils {
    public static void validateFile(String path) {
        File file = new File(path);
        failIfDirectory(file);
        failIfNotExist(file);
    }

    private static void failIfNotExist(File file) {
        if (!file.exists())
            throw new IllegalArgumentException("file not found.");
    }

    private static void failIfDirectory(File file) {
        if (file.isDirectory())
            throw new IllegalArgumentException("wrong file path.");
    }

    public static void failIfNull(String file) {
        if (file == null)
            throw new IllegalArgumentException("Null file not allowed.");
    }
}
