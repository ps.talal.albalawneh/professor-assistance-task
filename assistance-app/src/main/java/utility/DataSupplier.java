package utility;

import java.io.*;

public interface DataSupplier {

    String readPath();

    String getExtension();

    default void write(String data, String fileName) throws IOException {
        if (fileName == null)
            throw new NullPointerException("Null file name not allowed.");
        FileWriter fw = null;
        try {
            fw = new FileWriter("../result-files/" + fileName + this.getExtension());
            fw.write(data);
        } finally {
            fw.flush();
            fw.close();
        }
    }

    default String read() throws IOException {
        String path = this.readPath();
        FileUtils.validateFile(path);
        StringBuilder data = new StringBuilder();
        FileReader fr = null;
        try {
            fr = new FileReader(path);
            BufferedReader br = new BufferedReader(fr);
            String line;
            while ((line = br.readLine()) != null) {
                data.append(line).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            fr.close();
        }
        return data.toString();
    }

}
