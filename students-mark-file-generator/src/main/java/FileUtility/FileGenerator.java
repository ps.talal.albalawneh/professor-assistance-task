package FileUtility;

import java.io.IOException;

public interface FileGenerator {
    public void generate(String fileName, String delimiter) throws IOException;
}
