package FileUtility;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class TxtFile implements FileGenerator {

    @Override
    public void generate(String fileName, String delimiter) throws IOException {
        Random random = new Random();
        FileWriter fw = new FileWriter("./generated-files/" + fileName + ".txt");
        BufferedWriter bw = new BufferedWriter(fw);
        try {
            bw.write("student_id" + delimiter + "class_no" + delimiter + "mark\n");
            int lines = 120 + random.nextInt(100);
            char[] classNo = {'A', 'B', 'C'};
            int id = 202008001;
            for (int i = 0; i < lines; i++) {
                int cn = random.nextInt(3);
                int mark = random.nextInt(80) + 21;
                bw.write("" + id++ + delimiter + classNo[cn] + delimiter + mark);
                bw.write(i == lines - 1 ? "" : "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            bw.flush();
            bw.close();
            fw.close();
        }
    }
}

