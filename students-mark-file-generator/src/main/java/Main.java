import FileUtility.CSVFile;
import FileUtility.FileGenerator;
import FileUtility.TxtFile;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        FileGenerator fileGenerator = new CSVFile();
        fileGenerator.generate("csv_generated_file", ",");
    }
}
